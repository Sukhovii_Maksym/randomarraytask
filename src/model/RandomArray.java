package model;

import java.util.Random;

public class RandomArray {
    private final static int ARRAYLENGTH_X = 500;
    private final static int ARRAYLENGTH_Y = 300;
    private final static int MIN_REPLACE_AMOUNT = 300;
    private final static int MAX_REPLACE_AMOUNT = 500;
    private static byte[][] array;

    public static void start() {
        byte[][] buf = new byte[ARRAYLENGTH_Y][ARRAYLENGTH_X];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 5; j++) {
                buf[i][j] = 0;
            }
        }
        array = buf;
        print();
    }

    public static void print() {
        if (array == null) start();
        for (int i = 0; i < ARRAYLENGTH_Y; i++) {
            for (int j = 0; j < ARRAYLENGTH_X; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void restartAndModify() {
        start();
        long startTime = System.currentTimeMillis();
        Random random = new Random();
        int randomCount = random.nextInt(MAX_REPLACE_AMOUNT-MIN_REPLACE_AMOUNT) + MIN_REPLACE_AMOUNT+1;
        System.out.println("Program wants to change " + randomCount + " fields");
        while (randomCount > 0) {
            int index = random.nextInt(ARRAYLENGTH_X*ARRAYLENGTH_Y);
            int i = index / ARRAYLENGTH_X;
            int j = index % ARRAYLENGTH_X;
            if (array[i][j] == 0) {
                array[i][j] = 1;
                randomCount--;
            }
        }
        long endTime = System.currentTimeMillis();
        System.out.println("Time waste "+ (endTime-startTime));
        print();
    }
}
