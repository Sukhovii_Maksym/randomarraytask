package model;

import java.awt.Point;
import java.util.*;

public class TestExamp {
    private final static int ARRAYLENGTH_X = 500;
    private final static int ARRAYLENGTH_Y = 200;
    private final static int MIN_REPLACE_AMOUNT = 99999;
    private final static int MAX_REPLACE_AMOUNT = 100000;
    private static byte[][] array = new byte [ARRAYLENGTH_X][ARRAYLENGTH_Y];

    public static void main(String[] args) {

        int[][] arr = new int[ARRAYLENGTH_X][ARRAYLENGTH_Y];
        int[][] arr2 = new int[ARRAYLENGTH_X][ARRAYLENGTH_Y];
        List<Point> points = new ArrayList<>();

        //Init array and coordinates set
        for (int x = 0; x < ARRAYLENGTH_X; x++) {
            for (int y = 0; y < ARRAYLENGTH_Y; y++) {
                arr[x][y] = 0;
                arr2[x][y] = 0;
                array[x][y] = 0;
                points.add(new Point(x, y));
            }

        }
        long startTime = System.nanoTime();
        Random random = new Random();
        int numberReplace = random.ints(MIN_REPLACE_AMOUNT, (MAX_REPLACE_AMOUNT + 1)).findFirst().getAsInt();

        //Replace 0 to 1
        int replaceCounter = 0;
        while (replaceCounter < numberReplace) {
            int pointIndex = random.ints(0, points.size()).findFirst().getAsInt();
            Point p = points.get(pointIndex);
            arr[p.x][p.y] = 1;
            points.remove(pointIndex);
            replaceCounter++;
        }
        long endTime = System.nanoTime();
        System.out.println("Time waste 1st " + (endTime - startTime));
        System.out.println(Arrays.deepToString(arr));
        //-----------------------------------------
        startTime = System.nanoTime();
        int randomCount = random.nextInt(MAX_REPLACE_AMOUNT - MIN_REPLACE_AMOUNT) + MIN_REPLACE_AMOUNT + 1;
        System.out.println("Program wants to change " + randomCount + " fields");
        while (randomCount > 0) {
            int index = random.nextInt(ARRAYLENGTH_X * ARRAYLENGTH_Y );
            int i = index / ARRAYLENGTH_Y;
            int j = index % ARRAYLENGTH_Y;
            if (array[i][j] == 0) {
                array[i][j] = 1;
                randomCount--;

            }
        }
        endTime = System.nanoTime();
        System.out.println("Time waste 2nd " + (endTime - startTime));
        System.out.println(Arrays.deepToString(array));

        //-------------------------------------------
        startTime = System.nanoTime();

        List <Integer> list = new ArrayList<>();
        int size = ARRAYLENGTH_X*ARRAYLENGTH_Y;
        for (int i = 0; i < size; i++) {
            list.add(i,i);
        }
        randomCount = random.nextInt(MAX_REPLACE_AMOUNT - MIN_REPLACE_AMOUNT) + MIN_REPLACE_AMOUNT + 1;
        System.out.println("Program wants to change " + randomCount + " fields");
        int numberChanged = 0;
        while (randomCount > numberChanged) {
            int index = random.nextInt(size - numberChanged++);
            int value =list.get(index);
            list.remove(index);
            int i = value / ARRAYLENGTH_Y;
            int j = value % ARRAYLENGTH_Y;
                arr2[i][j] = 1;

            }
        endTime = System.nanoTime();
        System.out.println("Time waste 3d " + (endTime - startTime));
        System.out.println(Arrays.deepToString(arr2));

    }
}
